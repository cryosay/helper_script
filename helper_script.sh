#!/bin/bash -x

if [ ! -d cfg ]; then
	echo "---> helper_script: error: dir 'cfg' should be in the same place with the helper_script"
	return 1
fi

if [ ! -f cfg/cfg_common ]; then
	echo "---> helper_script: error: file 'cfg/cfg_common' not found"
	return 1
fi

source cfg/cfg_common

# default parameters

script_config_file=""

script_use_mkimg_standart_kernel=0

cfg_file_names=`bash -c "cd cfg && ls cfg_* | grep -v cfg_common"`

cfg_names=""

for entry in $cfg_file_names ; do
	cfg_names=$cfg_names${entry#cfg_}" "
done

cfg_names=${cfg_names# }

if [ "$cfg_names" = "" ]; then
	echo "---> helper_script: error: could not found any config file 'cfg/cfg_*'"
	return 1
fi

usage_str="---> Use: . helper_script"

cfg_count=0

for entry in $cfg_names ; do
	cfg_count=$(($cfg_count + 1))
	usage_str=$usage_str" ["$entry"]"
done

if [ $cfg_count -eq 1 ]; then
	echo "---> helper_script: only one configuration found: '$cfg_names' and it will be used"
	script_config_file=cfg_$cfg_names
else

	if [ $# -ne 1 ]; then
		echo "---> helper_script: please set configuration (should be one parameter)"
		echo "$usage_str"
		return
	else
		for entry in $cfg_names ; do
			if [ "$1" == "$entry" ]; then
				script_config_file=cfg_$entry
			fi
		done

		if [ "$script_config_file" == "" ]; then
			echo "$usage_str"
			return 1
		fi
	fi
fi

if [ ! -f cfg/$script_config_file ]; then
	echo "---> helper_script: error: file '$script_config_file' not found"
	return 1
fi

source cfg/$script_config_file

echo "---> helper_script: used cfg file: 'cfg/$script_config_file'"

if [ "${script_device_model}" == "PANDA5" ]; then

	script_fastboot_set_boot_slot="set_flash_slot:EMMC_BP"
	script_fastboot_set_emmc_slot="set_flash_slot:EMMC"
	script_fastboot_set_sata_slot="set_flash_slot:SATA"
else
	script_fastboot_set_boot_slot="spi"
	script_fastboot_set_emmc_slot="mmc"
	script_fastboot_set_sata_slot="sata"
fi

script_hypervisor_policy_dir=${script_hypervisor_dir}/tools/flask/policy
script_kernel_afs_dir=${script_afs_dir}/kernel/android-3.8

rootfs_dom_size=256

# should be defined after 'script_uboot_dir'
script_mkimage=${script_uboot_dir}/tools/mkimage
script_simg2img=${script_base_dir}/tools/simg2img
script_make_ext4fs=${script_base_dir}/tools/make_ext4fs

script_omapboot_out_dir=${script_omapboot_dir}/out/omap5uevm
script_hypervisor_out_dir=${script_hypervisor_dir}/xen

script_dom0_modules_install_dir=${script_rootfs_dom0_fs_dir}
script_domd_modules_install_dir=${script_rootfs_domd_fs_dir}
script_domt_modules_install_dir=${script_rootfs_domt_fs_dir}

script_kernel_dom_img_in_rootfs_dom0_dir=${script_rootfs_dom0_fs_dir}/xen/images

script_domu_cpio_file=${script_kernel_domu_src_dir}/arch/arm/boot/domUinitramfs.cpio
script_domu_cpio_dir=${script_kernel_domu_src_dir}/arch/arm/boot/_domUinitramfscpio

script_mkfs_ext4=mkfs.ext4

ccahe="ccache "

script_omapboot_toolchain=${ccahe}"arm-none-linux-gnueabi-"

script_uboot_toolchain=${ccahe}"arm-linux-gnueabihf-"

script_hypervisor_toolchain="arm-linux-gnueabihf-"

script_kernel_domu_toolchain=${ccahe}"arm-linux-gnueabihf-"
script_kernel_domc_toolchain=${ccahe}"arm-linux-gnueabihf-"
script_kernel_domc2_toolchain=${ccahe}"arm-linux-gnueabihf-"
script_kernel_dom0_toolchain=${ccahe}"arm-linux-gnueabihf-"
script_kernel_domd_toolchain=${ccahe}"arm-linux-gnueabihf-"
script_kernel_domt_toolchain=${ccahe}"arm-linux-gnueabihf-"

script_sgx_toolchain=${ccahe}"arm-linux-gnueabihf-"
script_afs_toolchain=""

script_pvaudio_toolchain=${ccahe}"arm-linux-gnueabihf-"
script_pvdrm_toolchain=${ccahe}"arm-linux-gnueabihf-"

# those settings should be made after the xen_kernel_domX_base_out_dir
[ "$script_kernel_dom0_base_out_dir" != "" ] && script_kernel_dom0_out_dir=${script_kernel_dom0_base_out_dir}/arch/arm/boot
[ "$script_kernel_domu_base_out_dir" != "" ] && script_kernel_domu_out_dir=${script_kernel_domu_base_out_dir}/arch/arm/boot
[ "$script_kernel_domc_base_out_dir" != "" ] && script_kernel_domc_out_dir=${script_kernel_domc_base_out_dir}/arch/arm/boot
[ "$script_kernel_domd_base_out_dir" != "" ] && script_kernel_domd_out_dir=${script_kernel_domd_base_out_dir}/arch/arm/boot
[ "$script_kernel_domt_base_out_dir" != "" ] && script_kernel_domt_out_dir=${script_kernel_domt_base_out_dir}/arch/arm/boot

# should be defined after 'xen_kernel_dom0_out_dir'
scripr_devtree_out_dir=${script_devtree_dir}

# those settings should be made after the script_afs_images_dir
script_kernel_afs_base_out_dir=${script_afs_images_dir}/obj/kernel
script_tmp_system_install_dir=${script_afs_images_dir}/_tmp_/system

__script_display_info() {
	echo "---> helper_script: device: '${script_device_model}'"
}

__script_display_info

# use __util_is_present file1 file2 ...
__util_is_present() {
	while [ "$1" ]
	do
		if [ "$(basename "${1}")" = "${1}" ]; then
			which ${1} >/dev/null
			if [ $? -eq 1 ]; then
				echo "---> Error: utility '${1}' not found"
				return 1
			fi
		else
			if [ ! -f ${1} ]; then
				echo "---> Error: file '${1}' does not exist"
				return 1
			fi
		fi
		shift
	done
	return 0
}

# use __dir_exists dir1 dir22 ...
__dir_exists() {
	while [ "$1" ]
	do
		if [ ! -d $1 ]; then
			echo "---> Error: directory '${1}' does not exist"
			return 1
		fi
		shift
	done
	return 0
}

# use __check_before_make
# SCRIPT_CUR_WORK_DIR must be exported
# if CROSS_COMPILE != "" the cross compiler is checked additionally
__check_before_make() {

	if [ "${SCRIPT_CUR_WORK_DIR}" = "" ]; then
		echo "---> Please, export SCRIPT_CUR_WORK_DIR before using __check_before_make()"
		return 1
	fi

	if ! __dir_exists ${SCRIPT_CUR_WORK_DIR} ; then
		return 1
	fi

	if [ "${CROSS_COMPILE}" != "" ]; then
		# do check toolchain
		if ! __util_is_present ${CROSS_COMPILE}"gcc" ; then
			return 1
		fi
	fi

	return 0
}

__script_check_root() {
	if [ "$(id -u)" != "0" ]; then
		echo "---> This function need root privilege. Please, enter root password:"
		sudo -S echo -n ''
		if [ $? -ne 0 ]; then
			echo "---> Error: wrong passord."
			return 1
		fi
		echo "---> Password entered..."
	fi
	return 0
}

# use: __script_copy_utils destination_dir util_1 util_2 ... util_n
__script_copy_utils() {
	local copy_path=""
	local copy_utils=""
	local cur_util=""
	local err_code=""

	if [ $# -lt 2 ]; then
		echo "---> ${FUNCNAME[ 1 ]}: need more parameters"
		return 1
	fi

	copy_path=$1
	shift

	while [ "$1" ]
	do
		copy_utils+=("$1")
		shift
	done

	if ! __util_is_present ${copy_utils[@]} ; then
		return 1
	fi

	if ! __dir_exists ${copy_path} ; then
		return 1
	fi

	for cur_util in ${copy_utils[@]} ; do
		echo "---> copy '$(basename "${cur_util}")' -> ${copy_path}"

		cp ${cur_util} ${copy_path}/
		err_code=$?

		if [ $err_code -eq 1 ]; then
			echo "---> warning: permission denied, try to copy with 'sudo'..."
			sudo bash -c "cp ${cur_util} ${copy_path}/"
			err_code=$?
		fi

		if [ $err_code -ne 0 ]; then
			echo "---> error while copying '$(basename "${cur_util}")' ..."
			return 1
		fi
	done

	return 0
}


# SCRIPT_CUR_WORK_DIR must be exported
# CROSS_COMPILE must be exported
__script_make_omapboot() {
	local usage_str="---> Usage: "${FUNCNAME[ 1 ]}" [do_clean]"
	local do_clean=0
	local build_name="omapboot xen"

	if ! __check_before_make ; then
		return 1
	fi

	while [ "$1" ]
	do
		case "$1" in
			do_clean) do_clean=1;;
			*) echo "$usage_str"; return 1;;
		esac
		shift
	done

	echo "---> "${build_name}" dir: '"${SCRIPT_CUR_WORK_DIR}"'"

	if [ $do_clean -ne 0 ];  then
		echo "---> clean "$build_name
		make -C ${SCRIPT_CUR_WORK_DIR} MACH=omap5 BOARD=omap5uevm BOOT=xen 2>&1 distclean
	fi

	echo "---> make "$build_name
	make -C ${SCRIPT_CUR_WORK_DIR} MACH=omap5 BOARD=omap5uevm BOOT=xen 2>&1

	if [ $? -ne 0 ]; then
		echo "---> make "$build_name" error..."
		return 1
	fi

	#echo "---> Coping files..."
	#cp ${script_omapboot_dir}/out/omap5uevm/omap5uevm_GP_ES1.0_MLO .
	#cp ${script_omapboot_dir}/out/omap5uevm/omap5uevm_GP_ES2.0_MLO .
	#cp ${script_omapboot_dir}/out/omap5uevm/usbboot .

	echo "---> Done "$build_name
	return 0
}

xen_make_omapboot() {
	__script_display_info

	export SCRIPT_CUR_WORK_DIR="${script_omapboot_dir}"
	export CROSS_COMPILE="${script_omapboot_toolchain}"

	__script_make_omapboot $@
}

# SCRIPT_CUR_WORK_DIR must be exported
# CROSS_COMPILE must be exported
__script_make_uboot() {
	local usage_str="---> Usage: "${FUNCNAME[ 1 ]}" [do_clean] [verbal]"
	local do_clean=0
	local build_name="u-boot xen"
	local silent="-s"

	if ! __check_before_make ; then
		return 1
	fi

	while [ "$1" ]
	do
		case "$1" in
			do_clean) do_clean=1;;
			verbal) silent="";;
			*) echo "$usage_str"; return 1;;
		esac
		shift
	done

	echo "---> WARNING: Check if 'make' command is needed after defconfig"
	echo "---> "${build_name}" dir: '"${SCRIPT_CUR_WORK_DIR}"'"

	if [ $do_clean -ne 0 ];  then
		echo "---> clean "$build_name
		make -C ${SCRIPT_CUR_WORK_DIR} 2>&1 -j${script_core_cnt} distclean
	fi

	echo "---> make "$build_name
	make -C ${SCRIPT_CUR_WORK_DIR} ${script_uboot_board} -j${script_core_cnt} ${silent} 2>&1

	if [ $? -ne 0 ]; then
		echo "---> make "$build_name" error..."
		return 1
	fi

	#echo "---> Coping files..."
	#cp ${script_omapboot_dir}/out/omap5uevm/omap5uevm_GP_ES1.0_MLO .
	#cp ${script_omapboot_dir}/out/omap5uevm/omap5uevm_GP_ES2.0_MLO .
	#cp ${script_omapboot_dir}/out/omap5uevm/usbboot .

	echo "---> Done "$build_name
	return 0
}

xen_make_uboot() {
	__script_display_info

	export SCRIPT_CUR_WORK_DIR="${script_uboot_dir}"
	export CROSS_COMPILE="${script_uboot_toolchain}"

	__script_make_uboot $@
}

# SCRIPT_CUR_WORK_DIR must be exported
# CROSS_COMPILE must be ""
__script_make_devtree() {
	local usage_str="---> Usage: "${FUNCNAME[ 1 ]}" [verbal] [do_clean]"
	local do_clean=0
	local verbal=""
	local build_name="devtree xen"

	if ! __check_before_make ; then
		return 1
	fi

	while [ "$1" ]
	do
		case "$1" in
			do_clean) do_clean=1;;
			verbal) verbal="KBUILD_VERBOSE=1";;
			*) echo "$usage_str"; return 1;;
		esac
		shift
	done

	echo "---> "${build_name}" dir: '"${SCRIPT_CUR_WORK_DIR}"'"

	if [ $do_clean -ne 0 ]; then
		echo "---> clean "$build_name
		make -C ${SCRIPT_CUR_WORK_DIR} ${verbal} 2>&1 clean
	fi

	echo "---> make "$build_name
	make -C ${SCRIPT_CUR_WORK_DIR} ${verbal} 2>&1

	if [ $? -ne 0 ]; then
		echo "---> make "$build_name" error..."
		return 1
	fi

	#echo "---> Coping files..."
	#cp ${script_devtree_dir}/omap5-panda.dtb .

	echo "---> Done "$build_name
	return 0
}

xen_make_devtree() {
	__script_display_info

	export SCRIPT_CUR_WORK_DIR="${script_devtree_dir}"
	export CROSS_COMPILE=""

	__script_make_devtree $@
}

# Usage: __script_mkimage uimage_name build_name mkimage_params
__script_mkimage() {
	local uimage_name=""
	local build_name=""

	if [ $# -lt 3 ]; then
		echo "---> ${FUNCNAME[ 1 ]}: need more parameters"
		return 1
	fi

	uimage_name=$1
	build_name=$2
	shift
	shift

	echo "---> creating "$uimage_name" in "$build_name
	${script_mkimage} $@
	if [ $? -ne 0 ]; then
		echo "---> creating "$uimage_name" in "$build_name" error..."
		return 1
	fi
}

# SCRIPT_CUR_WORK_DIR must be exported
# SCRIPT_HYPERVISOR_POLICY_DIR must be exported
# CROSS_COMPILE must be exported
__script_make_hypervisor() {
	local build_name="hypervisor"
	local usage_str="---> Usage: "${FUNCNAME[ 1 ]}" [do_clean] [do_mkimage] [xen] [dist_tools] [xenpolicy] [relocate_over_4gb] [debug] [verbal] [earlyprintk]"
	local saved_res=0
	local earlyprintk=""
	local do_mkimage=0
	local dist_tools=0
	local xen=0
	local debug="n"
	local do_clean=0
	local silent="-s"
	local xenpolicy=0
	local relocate_over_4gb_cmd=""

	if ! __check_before_make ; then
		return 1
	fi

	while [ "$1" ]
	do
		case "$1" in
			earlyprintk) earlyprintk="CONFIG_EARLY_PRINTK="$script_hypervisor_earlyprintk;;
			do_clean) do_clean=1;;
			do_mkimage) do_mkimage=1;;
			xen) xen=1;;
			dist_tools) dist_tools=1;;
			debug) debug="y";;
			verbal) silent="";;
			xenpolicy) xenpolicy=1;;
			relocate_over_4gb) relocate_over_4gb_cmd="ARM32_RELOCATE_OVER_4GB=y";;
			*) echo "$usage_str"; return 1;;
		esac
		shift
	done

	echo "---> "${build_name}" dir: '"${SCRIPT_CUR_WORK_DIR}"'"

	if [ $do_mkimage -eq 1 ]; then
		if ! __util_is_present ${script_mkimage} ; then
			return 1
		fi
	fi

	if [ $xenpolicy -ne 0 ]; then
		if ! __dir_exists ${SCRIPT_HYPERVISOR_POLICY_DIR} ; then
			return 1
		fi
	fi

	if [ $xen -eq 0 ] && [ $dist_tools -eq 0 ]  && [ $xenpolicy -eq 0 ]   && [ $do_mkimage -eq 0 ] ; then
		echo "---> "${build_name}" no target choozen ('xen' or 'dist_tools' or 'xenpolicy'). Chooze 'xen' target"
		xen=1
	fi

	if [ $do_clean -ne 0 ]; then
		echo "---> clean "$build_name
		make -C ${SCRIPT_CUR_WORK_DIR} distclean -j${script_core_cnt} ${silent} 2>&1

		saved_res=$?

		# there are some files that should be deleted by hands
		find ${SCRIPT_CUR_WORK_DIR}/xen -type f -name '*.*.d' -exec rm -f "{}" \;
		find ${SCRIPT_CUR_WORK_DIR}/xen -type f -name '*.o' -exec rm -f "{}" \;
		find ${SCRIPT_HYPERVISOR_POLICY_DIR} -maxdepth 1 -type f -name "xenpolicy*" \( ! -iname "*-uImage" \) -exec rm -f "{}" \;

		if [ $saved_res -ne 0 ]; then
			echo "---> make "$build_name" error..."
			return 1
		fi

	fi

	if [ $xen -ne 0 ]; then
		echo "---> make "$build_name" xen"
		# make with debug=y because bug in 4.6.3 toolchain
		make -C ${SCRIPT_CUR_WORK_DIR} xen CONFIG_QEMU_XEN=n XSM_ENABLE=y XEN_TARGET_ARCH=arm32 debug=$debug $earlyprintk $relocate_over_4gb_cmd -j${script_core_cnt} ${silent} 2>&1

		if [ $? -ne 0 ]; then
			echo "---> make "$build_name" xen error..."
			return 1
		fi
	fi

	if [ $do_mkimage -eq 1 ]; then
		if ! __script_mkimage xen-uImage "$build_name" -A arm -C none -T kernel -a 0x90000000 -e 0x90000000 -n "XEN" -d ${SCRIPT_CUR_WORK_DIR}/xen/xen ${SCRIPT_CUR_WORK_DIR}/xen/xen-uImage ; then
			return 1
		fi
	fi

	if [ $dist_tools -ne 0 ] || [ $xenpolicy -ne 0 ]; then
		echo "---> configure "$build_name""
		bash -c 'cd '${SCRIPT_CUR_WORK_DIR}' && ./configure --build=x86_64-unknown-linux-gnu --host=arm-linux-gnueabihf'

		if [ $? -ne 0 ]; then
			echo "---> configure "$build_name" error..."
			return 1
		fi
	fi

	if [ $xenpolicy -ne 0 ]; then
		echo "---> make "$build_name" xenpolicy"
		make -C ${SCRIPT_HYPERVISOR_POLICY_DIR}

		if [ $? -ne 0 ]; then
			echo "---> make "$build_name" xen error..."
			return 1
		fi
	fi

	if [ $dist_tools -ne 0 ]; then
		echo "---> make "$build_name" dist-tools"
		make -C ${SCRIPT_CUR_WORK_DIR} dist-tools CONFIG_QEMU_XEN=n XEN_TARGET_ARCH=arm32 debug=$debug -j${script_core_cnt} ${silent} 2>&1

		if [ $? -ne 0 ]; then
			echo "---> make "$build_name" dist-tools error..."
			return 1
		fi
	fi

	#echo "---> Coping files..."
	#cp ${script_hypervisor_dir}/xen/xen.bin .

	echo "---> Done "$build_name
	return 0
}

xen_make_hypervisor() {
	__script_display_info

	export SCRIPT_HYPERVISOR_POLICY_DIR="${script_hypervisor_policy_dir}"
	export SCRIPT_CUR_WORK_DIR="${script_hypervisor_dir}"
	export CROSS_COMPILE="${script_hypervisor_toolchain}"

	__script_make_hypervisor $@
}

# Usage: __script_copy_kernel_to_fs kernel_base_out_dir copy_kernel_type
# copy_kernel_type can be "D" or "U" or "T"
__script_copy_kernel_to_fs() {
	local zimage=${1}/arch/arm/boot/zImage
	local devtree_target_name=script_devtree_dom${2,,}_target
	local dtb=${1}/arch/arm/boot/dts/${!devtree_target_name}
	local kernel_type=${2}
	local add_media_warn=""
	local err_code=0

	if [ "${!devtree_target_name}" == "" ]; then
		echo "---> error: variable '"${devtree_target_name}"' is not set in script"
		return 1
	fi

	if [ ! -d ${script_kernel_dom_img_in_rootfs_dom0_dir} ]; then
		echo "---> error: path '"${script_kernel_dom_img_in_rootfs_dom0_dir}"' is not found"
		return 1
	fi

	if ! __util_is_present ${zimage} ${dtb} ; then
		return 1
	fi

	echo "---> copy zImage with appended '"$(basename $dtb)"' to '"${script_kernel_dom_img_in_rootfs_dom0_dir}"'"
	cat ${zimage} ${dtb} > ${script_kernel_dom_img_in_rootfs_dom0_dir}/Dom${kernel_type}Linux.img
	err_code=$?

	if [ $err_code -eq 1 ]; then
		echo "---> warning: permission denied, try to copy with 'sudo'..."
		sudo bash -c "cat ${zimage} ${dtb} > ${script_kernel_dom_img_in_rootfs_dom0_dir}/Dom${kernel_type}Linux.img"
		err_code=$?
	fi

	if [ $err_code -ne 0 ]; then
		echo "---> error: can not copy dom"$kernel_type
		return 1
	fi

	return 0
}

# Usage: __script_make_kernel [clean] [dom0_defconfig | domU_defconfig]
# SCRIPT_CUR_WORK_DIR must be exported
# CROSS_COMPILE must be exported
# SCRIPT_BUILD_NAME must be exported
# SCRIPT_KERNEL_BASE_OUT_DIR must be exported
# SCRIPT_MODULES_INSTALL_DIR must be exported for 'modules_install' parameter
# SCRIPT_HEADERS_INSTALL_DIR must be exported for 'headers_install' parameter
__script_make_kernel() {
	local do_clean=0
	local defconfig=""
	local devtree=""
	local do_make=0
	local menuconfig=""
	local do_mrproper=0
	local add_build_param=""
	local verbal=""
	local do_make_modules=0
	local do_headers_install=0
	local do_modules_install=0
	local modules_dir=""
	local copy_to_fs=0
	local do_mkimage=0
	local err_code=0
	local use_sudo=0
	local savedefconfig=0
	local oldconfig=0
	local func_name=${FUNCNAME[ 1 ]}
	local usage_str="---> Usage: "$func_name" [do_clean] [do_mkimage] [do_mrproper] [*_defconfig] [do_*config] [do_make] [copy_to_fs] ..."$'\n'
	local copy_kernel_type=""
	local add_usage_str=""

	usage_str=$usage_str"--->    ... [do_savedefconfig] [do_oldconfig] [process_modules] [modules] [modules_install] [headers_install] [*.dtb] [dtbs] [verbal]"$'\n'
	usage_str=$usage_str"---> some menuconfigs: do_nconfig do_menucongig do_xconfig"

	if [[ "$func_name" =~ dom0 ]]; then
		add_usage_str=$script_kernel_dom0_info
	elif [[ "$func_name" =~ domD ]]; then
		add_usage_str=$script_kernel_domd_info
		copy_kernel_type="D"
	elif [[ "$func_name" =~ domU ]]; then
		add_usage_str=$script_kernel_domu_info
		copy_kernel_type="U"
	elif [[ "$func_name" =~ domC2 ]]; then
		add_usage_str=$script_kernel_domc2_info
		copy_kernel_type="C2"
	elif [[ "$func_name" =~ domC ]]; then
		add_usage_str=$script_kernel_domc_info
		copy_kernel_type="C"
	elif [[ "$func_name" =~ domT ]]; then
		add_usage_str=$script_kernel_domt_info
		copy_kernel_type="T"
	fi

	if [ "$add_usage_str" == "" ]; then
		echo "---> function '$func_name' is not allowed for the '$script_device_model' configuration"
		return 1
	fi

	if ! __check_before_make ; then
		return 1
	fi

	usage_str=$usage_str$'\n'$add_usage_str

	if [ $# -eq 0 ]; then
		echo "---> No given parametrs. Just do make."
		do_make=1
	else
		while [ "$1" ]
		do
			case "$1" in
				do_clean) do_clean=1;;
				do_menuconfig) menuconfig="menuconfig";;
				do_nconfig) menuconfig="nconfig";;
				do_xconfig) menuconfig="xconfig";;
				do_mkimage) do_mkimage=1;;
				do_make) do_make=1;;
				process_modules)  do_make_modules=1; do_modules_install=1;;
				modules) do_make_modules=1;;
				modules_install) do_modules_install=1;;
				headers_install) do_headers_install=1;;
				do_mrproper) do_mrproper=1;;
				do_savedefconfig) savedefconfig=1;;
				do_oldconfig) oldconfig=1;;
				*_defconfig) defconfig="$1";;
				*.dtb) devtree="$devtree $1";;
				dtbs) devtree="$devtree $1";;
				verbal) verbal="V=1";;
				copy_to_fs) copy_to_fs=1;;
				*) echo "$usage_str"; return 1;;
			esac
			shift
		done
	fi

	echo "---> "${SCRIPT_BUILD_NAME}" src dir: '"${SCRIPT_CUR_WORK_DIR}"'"
	[ "$SCRIPT_CUR_WORK_DIR" != "$SCRIPT_KERNEL_BASE_OUT_DIR" ] && echo "---> "${SCRIPT_BUILD_NAME}" out dir: '"${SCRIPT_KERNEL_BASE_OUT_DIR}"'"

	if [ $do_mkimage -eq 1 ]; then
		if ! __util_is_present ${script_mkimage} ; then
			return 1
		fi
	fi

	if [ $copy_to_fs -eq 1 ] && [ "$copy_kernel_type" == "" ] ; then
		echo "---> Error: copy_to_fs parameter can be used only for domD or DomU kernel"
		return 1
	fi

	if [ "$SCRIPT_CUR_WORK_DIR" != "$SCRIPT_KERNEL_BASE_OUT_DIR" ]; then
		add_build_param="O=$SCRIPT_KERNEL_BASE_OUT_DIR"
		if [ ! -d ${SCRIPT_KERNEL_BASE_OUT_DIR} ]; then
			mkdir -p ${SCRIPT_KERNEL_BASE_OUT_DIR}
			if [ $? -ne 0 ]; then
				echo "---> make "$SCRIPT_BUILD_NAME" error: can not create dir '${SCRIPT_KERNEL_BASE_OUT_DIR}'"
				return 1
			fi
		fi
	fi

	if [ $do_modules_install -eq 1 ]; then
		if [ "$SCRIPT_MODULES_INSTALL_DIR" == "" ]; then
			echo "---> make "$SCRIPT_BUILD_NAME" error: SCRIPT_MODULES_INSTALL_DIR should be exported to use 'process_modules' or 'modules_install'"
			return 1
		fi

		if [ -d ${SCRIPT_MODULES_INSTALL_DIR} ]; then
			modules_dir=${SCRIPT_MODULES_INSTALL_DIR}/lib/modules

			echo "---> Deleting dir '${modules_dir}'"

			if [ -d ${modules_dir} ]; then
				rm -Rf ${modules_dir}
				err_code=$?

				if [ $err_code -eq 1 ]; then
					echo "---> warning: permission denied, try to delete with 'sudo'..."
					use_sudo=1
					sudo rm -Rf ${modules_dir}
					err_code=$?
				fi

				if [ $err_code -ne 0 ]; then
					echo "---> Error deleting modules dir: '${modules_dir}'"
					return 1
				fi
			fi
		else
			mkdir -p ${SCRIPT_MODULES_INSTALL_DIR}
			err_code=$?

			if [ $err_code -eq 1 ]; then
				echo "---> warning: permission denied, try to create with 'sudo'..."
				use_sudo=1
				sudo mkdir -p ${SCRIPT_MODULES_INSTALL_DIR}
				err_code=$?
			fi

			if [ $err_code -ne 0 ]; then
				echo "---> make "$SCRIPT_BUILD_NAME" error: can not create dir for kernel modules '${SCRIPT_MODULES_INSTALL_DIR}'"
				return 1
			fi
		fi
	fi

	if [ $do_headers_install -eq 1 ]; then
		if [ "$SCRIPT_HEADERS_INSTALL_DIR" == "" ]; then
			echo "---> make "$SCRIPT_BUILD_NAME" error: SCRIPT_HEADERS_INSTALL_DIR should be exported to use 'headers_install'"
			return 1
		fi

		if [ -d ${SCRIPT_HEADERS_INSTALL_DIR} ]; then
			echo "---> deleting dir for kernel headers '${SCRIPT_HEADERS_INSTALL_DIR}'"
			rm -Rf ${SCRIPT_HEADERS_INSTALL_DIR}
			if [ $? -ne 0 ]; then
				echo "---> make "$SCRIPT_BUILD_NAME" error: can not delete dir for kernel headers '${SCRIPT_HEADERS_INSTALL_DIR}'"
				return 1
			fi
		fi
	fi

	if [ $do_mrproper -eq 1 ]; then
		echo "---> mrproper "$SCRIPT_BUILD_NAME
		# we do not need ${add_build_param} here because mrproper should be done in the kernel source dir
		make -C ${SCRIPT_CUR_WORK_DIR} ARCH=arm 2>&1 -j${script_core_cnt} mrproper ${verbal}
	fi

	if [ $do_clean -eq 1 ]; then
		echo "---> clean "$SCRIPT_BUILD_NAME
		make -C ${SCRIPT_CUR_WORK_DIR} ARCH=arm 2>&1 -j${script_core_cnt} distclean ${add_build_param} ${verbal}
	fi

	if [ "$defconfig" != "" ]; then
		echo "---> make "$SCRIPT_BUILD_NAME" "$defconfig
		make -C ${SCRIPT_CUR_WORK_DIR} ARCH=arm 2>&1 $defconfig ${add_build_param} ${verbal}
		if [ $? -ne 0 ]; then
			echo "---> make "$SCRIPT_BUILD_NAME" "$defconfig" error..."
			return 1
		fi

	fi

	if [ "$menuconfig" != "" ]; then
		echo "---> menuconfig "$SCRIPT_BUILD_NAME
		make -C ${SCRIPT_CUR_WORK_DIR} ARCH=arm 2>&1 ${menuconfig} ${add_build_param} ${verbal}
	fi

	if [ $savedefconfig -eq 1 ]; then
		echo "---> savedefconfig "$SCRIPT_BUILD_NAME
		make -C ${SCRIPT_CUR_WORK_DIR} ARCH=arm 2>&1 -j${script_core_cnt} savedefconfig ${add_build_param} ${verbal}
	fi

	if [ $oldconfig -eq 1 ]; then
		echo "---> oldconfig "$SCRIPT_BUILD_NAME
		make -C ${SCRIPT_CUR_WORK_DIR} ARCH=arm 2>&1 -j${script_core_cnt} oldconfig ${add_build_param} ${verbal}
	fi

	if [ "$devtree" != "" ]; then
		echo "---> make "$SCRIPT_BUILD_NAME" "$devtree
		make -C ${SCRIPT_CUR_WORK_DIR} ARCH=arm 2>&1 $devtree ${add_build_param} ${verbal}
		if [ $? -ne 0 ]; then
			echo "---> make "$SCRIPT_BUILD_NAME" "$devtree" error..."
			return 1
		fi
	fi

	if [ $do_make -eq 1 ]; then
		echo "---> make "$SCRIPT_BUILD_NAME
		make -C ${SCRIPT_CUR_WORK_DIR} ARCH=arm 2>&1 -j${script_core_cnt} zImage ${add_build_param} ${verbal}

		if [ $? -ne 0 ]; then
			echo "---> make "$SCRIPT_BUILD_NAME" error..."
			return 1
		fi
	fi

	if [ $do_make_modules -eq 1 ]; then
		echo "---> make modules "$SCRIPT_BUILD_NAME
		make -C ${SCRIPT_CUR_WORK_DIR} ARCH=arm 2>&1 -j${script_core_cnt} modules ${add_build_param} ${verbal}

		if [ $? -ne 0 ]; then
			echo "---> make "$SCRIPT_BUILD_NAME" error..."
			return 1
		fi
	fi

	if [ $do_modules_install -eq 1 ]; then
		echo "---> modules_install "$SCRIPT_BUILD_NAME
		echo "---> modules install dir: "$SCRIPT_MODULES_INSTALL_DIR
		if [ $use_sudo -ne 0 ]; then
			sudo make -C ${SCRIPT_CUR_WORK_DIR} ARCH=arm 2>&1 -j${script_core_cnt} modules_install ${add_build_param} INSTALL_MOD_PATH=${SCRIPT_MODULES_INSTALL_DIR} ${verbal}
			sudo chmod -R 757 ${SCRIPT_MODULES_INSTALL_DIR}/lib/modules
		else
			make -C ${SCRIPT_CUR_WORK_DIR} ARCH=arm 2>&1 -j${script_core_cnt} modules_install ${add_build_param} INSTALL_MOD_PATH=${SCRIPT_MODULES_INSTALL_DIR} ${verbal}
		fi

		if [ $? -ne 0 ]; then
			echo "---> make "$SCRIPT_BUILD_NAME" error..."
			return 1
		fi
	fi

	if [ $do_headers_install -eq 1 ]; then
		echo "---> headers_install "$SCRIPT_BUILD_NAME
		echo "---> headers install dir: "$SCRIPT_HEADERS_INSTALL_DIR
		make -C ${SCRIPT_CUR_WORK_DIR} ARCH=arm 2>&1 -j${script_core_cnt} headers_install ${add_build_param} INSTALL_HDR_PATH=${SCRIPT_HEADERS_INSTALL_DIR} ${verbal}

		if [ $? -ne 0 ]; then
			echo "---> make "$SCRIPT_BUILD_NAME" error..."
			return 1
		fi
	fi

	if [ $do_mkimage -eq 1 ]; then
		# parameters for Dragon2
		#${script_mkimage} -A arm -C none -T kernel -a 0x80300000 -e 0x80300000 -n "kernel" -d $SCRIPT_KERNEL_BASE_OUT_DIR/arch/arm/boot/zImage $SCRIPT_KERNEL_BASE_OUT_DIR/arch/arm/boot/kernel.uImage
		if ! __script_mkimage zImage-uImage "$SCRIPT_BUILD_NAME" -A arm -C none -T filesystem -n "DOM0" -d $SCRIPT_KERNEL_BASE_OUT_DIR/arch/arm/boot/zImage $SCRIPT_KERNEL_BASE_OUT_DIR/arch/arm/boot/zImage-uImage ; then
			return 1
		fi
	fi

	if [ $copy_to_fs -eq 1 ]; then
		if ! __script_copy_kernel_to_fs ${SCRIPT_KERNEL_BASE_OUT_DIR} ${copy_kernel_type} ; then
			return 1
		fi
	fi

	#echo "---> Coping files..."
	#cp ${local_kernel_dir}/arch/arm/boot/zImage .

	echo "---> Done "$SCRIPT_BUILD_NAME
	return 0
}

xen_make_kernel_dom0() {
	__script_display_info

	export SCRIPT_CUR_WORK_DIR="${script_kernel_dom0_src_dir}"
	export CROSS_COMPILE="${script_kernel_dom0_toolchain}"
	export SCRIPT_KERNEL_BASE_OUT_DIR="${script_kernel_dom0_base_out_dir}"
	export SCRIPT_BUILD_NAME="kernel dom0"
	export SCRIPT_MODULES_INSTALL_DIR="${script_dom0_modules_install_dir}"
	export SCRIPT_HEADERS_INSTALL_DIR="${script_kernel_dom0_headers_install_dir}"

	__script_make_kernel $@
}

xen_make_kernel_domD() {
	__script_display_info

	export SCRIPT_CUR_WORK_DIR="${script_kernel_domd_src_dir}"
	export CROSS_COMPILE="${script_kernel_domd_toolchain}"
	export SCRIPT_KERNEL_BASE_OUT_DIR="${script_kernel_domd_base_out_dir}"
	export SCRIPT_BUILD_NAME="kernel domD"
	export SCRIPT_MODULES_INSTALL_DIR="${script_domd_modules_install_dir}"
	export SCRIPT_HEADERS_INSTALL_DIR="${script_kernel_domd_headers_install_dir}"

	__script_make_kernel $@
}

xen_make_kernel_domU() {
	__script_display_info

	export SCRIPT_CUR_WORK_DIR="${script_kernel_domu_src_dir}"
	export CROSS_COMPILE="${script_kernel_domu_toolchain}"
	export SCRIPT_KERNEL_BASE_OUT_DIR="${script_kernel_domu_base_out_dir}"
	export SCRIPT_BUILD_NAME="kernel domU"
	export SCRIPT_MODULES_INSTALL_DIR=""
	export SCRIPT_HEADERS_INSTALL_DIR="${script_kernel_domu_headers_install_dir}"

	__script_make_kernel $@
}

xen_make_kernel_domC() {
	__script_display_info

	export SCRIPT_CUR_WORK_DIR="${script_kernel_domc_src_dir}"
	export CROSS_COMPILE="${script_kernel_domc_toolchain}"
	export SCRIPT_KERNEL_BASE_OUT_DIR="${script_kernel_domc_base_out_dir}"
	export SCRIPT_BUILD_NAME="kernel domC"
	export SCRIPT_MODULES_INSTALL_DIR=""
	export SCRIPT_HEADERS_INSTALL_DIR="${script_kernel_domc_headers_install_dir}"

	__script_make_kernel $@
}

xen_make_kernel_domC2() {
	__script_display_info

	export SCRIPT_CUR_WORK_DIR="${script_kernel_domc2_src_dir}"
	export CROSS_COMPILE="${script_kernel_domc2_toolchain}"
	export SCRIPT_KERNEL_BASE_OUT_DIR="${script_kernel_domc2_base_out_dir}"
	export SCRIPT_BUILD_NAME="kernel domC2"
	export SCRIPT_MODULES_INSTALL_DIR=""
	export SCRIPT_HEADERS_INSTALL_DIR="${script_kernel_domc2_headers_install_dir}"

	__script_make_kernel $@
}

xen_make_kernel_domT() {
	__script_display_info

	export SCRIPT_CUR_WORK_DIR="${script_kernel_domt_src_dir}"
	export CROSS_COMPILE="${script_kernel_domt_toolchain}"
	export SCRIPT_KERNEL_BASE_OUT_DIR="${script_kernel_domt_base_out_dir}"
	export SCRIPT_BUILD_NAME="kernel domT"
	export SCRIPT_MODULES_INSTALL_DIR="${script_domt_modules_install_dir}"
	export SCRIPT_HEADERS_INSTALL_DIR="${script_kernel_domt_headers_install_dir}"

	__script_make_kernel $@
}

xen_make_kernel_afs() {
	__script_display_info

	export SCRIPT_CUR_WORK_DIR="${script_kernel_afs_dir}"
	export CROSS_COMPILE="${script_afs_toolchain}"
	export SCRIPT_KERNEL_BASE_OUT_DIR="${script_kernel_afs_base_out_dir}"
	export SCRIPT_BUILD_NAME="kernel AFS"
	export SCRIPT_MODULES_INSTALL_DIR=""
	export SCRIPT_HEADERS_INSTALL_DIR="${script_kernel_headers_install_dir}"

	__script_make_kernel $@
}

__script_usbbot() {
	echo "---> usbboot dir: '"${SCRIPT_CUR_WORK_DIR}"'"

	${SCRIPT_CUR_WORK_DIR}/usbboot -f
}

xen_usbbot() {
	__script_display_info

	export SCRIPT_CUR_WORK_DIR="${script_omapboot_out_dir}"
	__script_usbbot
}

# SCRIPT_AFS_IMAGES_DIR must be exported
# SCRIPT_ROOTSF_MAKE_EXT4FS must be exported
# usage: __script_create_rootfs_dom0_img <SCRIPT_ROOTSF_DOMx_DIR> <dom>
# where <dom> can be '0' or 'd'
__script_create_rootfs_dom_img() {
	local rootfs_dom_img=${SCRIPT_AFS_IMAGES_DIR}"/rootfs_dom"${2}".img"
	local tmp_rootfs_dom_dir=${SCRIPT_AFS_IMAGES_DIR}"/_rootfs_dom"${2}
	local my_user="`id -u -n`"

	if [ "${2}" != "0" ] && [ "${2}" != "d" ]; then
		echo "---> Wrong dom parameter..."
		return 1
	fi

	if ! __util_is_present ${SCRIPT_ROOTSF_MAKE_EXT4FS} ; then
		return 1
	fi

	if ! __dir_exists ${SCRIPT_AFS_IMAGES_DIR} ${1} ; then
		return 1
	fi

	echo "---> Creating rootfs_dom"${2}" ext4 image..."
	dd if=/dev/zero of=${rootfs_dom_img} bs=1M count=${rootfs_dom_size}
	${SCRIPT_ROOTSF_MAKE_EXT4FS} -F ${rootfs_dom_img}

	echo "---> Packing rootfs..."
	mkdir -p ${tmp_rootfs_dom_dir}
	sudo mount -o loop -t ext4 ${rootfs_dom_img} ${tmp_rootfs_dom_dir}
	sudo rm -rf ${tmp_rootfs_dom_dir}/*
	sudo cp -a ${1}/* ${tmp_rootfs_dom_dir}/
	sudo chown -R root:root ${tmp_rootfs_dom_dir}
	sync
	sudo umount ${tmp_rootfs_dom_dir}

	sudo chown $my_user:$my_user ${rootfs_dom_img}
	sudo rm -Rf ${tmp_rootfs_dom_dir}

	echo "---> Done"
	return 0
}

# Usage: __script_fastboot_do_flash_cmd <partition> <f_name>
__script_fastboot_do_flash_cmd() {
	if [ "${2}" != "" ]; then
		echo "---> xen fastboot flash ${1} '$(basename "${2}")'..."
		echo "---> ${1}: ["${2}"]"
		${SCRIPT_FASTBOOT} flash ${1} ${2}

		if [ $# -eq 0 ]; then
			echo "---> Error flashing '${1}'"
			return 1
		fi
	fi
	return 0
}

# Usage: __script_fastboot_do_oem_cmd <partition_cmd>
__script_fastboot_do_oem_cmd() {
	echo "---> xen fastboot oem ${1}..."
	${SCRIPT_FASTBOOT} oem ${1}

	if [ $# -eq 0 ]; then
		echo "---> Error oem command '${1}'"
		return 1
	fi
	return 0
}

# SCRIPT_KERNEL_OUT_DIR must be exported
# SCRIPT_OMAPBOOT_OUT_DIR must be exported
# SCRIPT_UBOOT_OUT_DIR must be exported
# SCRIPT_HYPERVISOR_OUT_DIR must be exported
# SCRIPT_DEVTREE_OUT_DIR must be exported
# SCRIPT_FASTBOOT must be exported
# SCRIPT_MKIMAGE must be exported
# SCRIPT_AFS_IMAGES_DIR must be exported
# SCRIPT_MAKE_EXT4FS must be exported
# SCRIPT_ROOTSF_MAKE_EXT4FS must be exported
# SCRIPT_ROOTSF_DOM0_DIR must be exported
# SCRIPT_ROOTSF_DOMD_DIR must be exported
__script_fastboot() {
	local do_oem=0
	local devtree=""
	local hypervisor=""
	local omlo=""
	local umlo=""
	local uboot=""
	local kernel_dom0=""
	local kernel_domd=""
	local system=""
	local userdata=""
	local cache=""
	local efs=""
	local animation=""
	local logo=""
	local xenpolicy=""
	local slot_warn_str="---> Error: only one device slot can be flashed at a time."
	local kernel_warn_str="---> Error: kernel_zImage and kernel_Image can not be flashed together."
	local devtree_warn_str="---> Error: devtree and thin_devtree can not be flashed together."
	local eraseuboot=0
	local do_mkimg=0
	local do_wrire_op=0
	local tmp
	local devtree_partition_name="environment"
	local rootfs_dom0_partition_name="rootfs_dom0"
	local rootfs_domd_partition_name="rootfs_domd"
	local set_slot=""
	local do_set_boot_slot=0
	local do_create_cache=0
	local do_create_efs=0
	local rootfs_dom0=""
	local rootfs_domd=""

	local MKIMGFLAGS_DT="-A arm -C none -T filesystem -n DTB"
	local MKIMGFLAGS_ZIMG="-A arm -C none -T filesystem -n DOM0"
	local MKIMGFLAGS_XEN="-A arm -C none -T kernel -a 0x90000000 -e 0x90000000 -n XEN"
	local MKIMGFLAGS_LOGO="-A arm -C none -T filesystem -n LOGO"
	local MKIMGFLAGS_XENPOLICY="-A arm -C none -T filesystem -n XP"

	local usage_str="---> Usage: "${FUNCNAME[ 1 ]}" [qspi_slot] [bp0_slot] [emmc_slot] [sata_slot] [oem] [eraseuboot] ..."$'\n'

	usage_str=$usage_str"    ... [uboot_mlo] [uboot_img] [omapboot] ..."$'\n'
	usage_str=$usage_str"    ... [devtree] [thin_devtree] [hypervisor] [xenpolicy] [dom0_zImage] [dom0_Image] [domd_zImage] [domd_Image] [logo] [mkimg] ..."$'\n'
	usage_str=$usage_str"    ... [rootfs_dom0] [rootfs_domd] [system] [userdata] [cache] [efs] [animation]"$'\n'
	usage_str=$usage_str"---> Tip: just for create uImage-files don't use [qspi/bp0/emmc/sata]_slot parametr"

	if ! __util_is_present ${SCRIPT_FASTBOOT} ; then
		return 1
	fi

	if [ $# -eq 0 ]; then
		echo "$usage_str"
		return 1
	fi

	while [ "$1" ]
	do
		case "$1" in
			oem) do_oem=1;;
			omapboot) omlo=${SCRIPT_OMAPBOOT_OUT_DIR}"/omap5uevm_GP_ES2.0_MLO";;
			uboot_mlo) umlo=${SCRIPT_UBOOT_OUT_DIR}"/MLO";;
			uboot_img) uboot=${SCRIPT_UBOOT_OUT_DIR}/"u-boot.img";;
			hypervisor) hypervisor=${SCRIPT_HYPERVISOR_OUT_DIR}"/xen";;
			devtree)  if [ "$devtree" == "" ]; then devtree=${SCRIPT_DEVTREE_OUT_DIR}/${script_devtree_dom0_target}; else echo "$devtree_warn_str"; return 1; fi;;
			thin_devtree)  if [ "$devtree" == "" ]; then devtree=${SCRIPT_DEVTREE_OUT_DIR}/${script_devtree_thin_dom0_target}; else echo "$devtree_warn_str"; return 1; fi;;
			dom0_zImage) if [ "$kernel_dom0" == "" ]; then kernel_dom0=${SCRIPT_KERNEL_DOM0_OUT_DIR}"/zImage"; else echo "$kernel_warn_str"; return 1; fi;;
			dom0_Image)  if [ "$kernel_dom0" == "" ]; then kernel_dom0=${SCRIPT_KERNEL_DOM0_OUT_DIR}"/Image"; else echo "$kernel_warn_str"; return 1; fi;;
			domd_zImage) if [ "$kernel_domd" == "" ]; then kernel_domd=${SCRIPT_KERNEL_DOMD_OUT_DIR}"/zImage"; else echo "$kernel_warn_str"; return 1; fi;;
			domd_Image)  if [ "$kernel_domd" == "" ]; then kernel_domd=${SCRIPT_KERNEL_DOMD_OUT_DIR}"/Image"; else echo "$kernel_warn_str"; return 1; fi;;
			eraseuboot) eraseuboot=1;;
			mkimg) do_mkimg=1;;
			system) system=${SCRIPT_AFS_IMAGES_DIR}"/system.img";;
			userdata) userdata=${SCRIPT_AFS_IMAGES_DIR}"/userdata.img";;
			cache) cache=${SCRIPT_AFS_IMAGES_DIR}"/cache.img";;
			rootfs_dom0) rootfs_dom0=${SCRIPT_AFS_IMAGES_DIR}"/rootfs_dom0.img";;
			rootfs_domd) rootfs_domd=${SCRIPT_AFS_IMAGES_DIR}"/rootfs_domd.img";;
			efs) efs=${SCRIPT_AFS_IMAGES_DIR}"/efs.img";;
			animation) animation=${SCRIPT_AFS_IMAGES_DIR}"/boot_10.bas";;
			logo) logo=${SCRIPT_AFS_IMAGES_DIR}"/logo.raw";;
			xenpolicy) xenpolicy="dummy";;
			qspi_slot) if [ "$set_slot" == "" ]; then set_slot=$script_fastboot_set_boot_slot; do_set_boot_slot=1; else echo "$slot_warn_str"; return 1; fi;;
			bp0_slot) if [ "$set_slot" == "" ]; then set_slot=$script_fastboot_set_boot_slot; do_set_boot_slot=1; else echo "$slot_warn_str"; return 1; fi;;
			emmc_slot) if [ "$set_slot" == "" ]; then set_slot=$script_fastboot_set_emmc_slot; else echo "$slot_warn_str"; return 1; fi;;
			sata_slot) if [ "$set_slot" == "" ]; then set_slot=$script_fastboot_set_sata_slot; else echo "$slot_warn_str"; return 1; fi;;
			*) echo "$usage_str"; return 1;;
		esac
		shift
	done

	if [ "$xenpolicy" != "" ]; then
		xenpolicy=`find ${SCRIPT_HYPERVISOR_POLICY_DIR} -maxdepth 1 -type f -name "xenpolicy*" \( ! -iname "*-uImage" \) -print -quit`
		if [ "${xenpolicy}" == "" ]; then
			echo "---> xenpolicy file not found"
			return 1
		fi
	fi

	if [ $script_use_mkimg_standart_kernel -ne 0 ]; then
		MKIMGFLAGS_ZIMG="-A arm -O linux -T kernel -C none -a 0x80008000 -e 0x80008000 -n \"Linux Kernel Image\""
	fi

	if [ $do_mkimg -ne 0 ]; then
		if ! __util_is_present ${SCRIPT_MKIMAGE} ; then
			return 1
		fi
	fi

	if [ "$omlo" != "" ]; then
		if [ "$umlo" != "" ] || [ "$uboot" != "" ]; then
			echo "---> u-boot and omapboot can not be flashed at the same time"
			return 1
		fi
	fi

	if [ "$rootfs_dom0" != "" ]; then
		if ! __dir_exists  ${SCRIPT_ROOTSF_DOM0_DIR} ; then
			return 1
		fi
	fi

	if [ "$rootfs_domd" != "" ]; then
		if ! __dir_exists  ${SCRIPT_ROOTSF_DOMD_DIR} ; then
			return 1
		fi
	fi

	# change devtree name and partition according to the board
	if [ "${script_device_model}" == "PANDA5" ]; then
		devtree_partition_name="device_tree"
		rootfs_dom0_partition_name="system"
	fi

	if [ $do_oem -eq 1 ] && [ $do_set_boot_slot -eq 1 ]; then
		echo "---> Error: 'oem' can not be used together with 'qspi_slot' or 'bp0_slot'"
		return 1
	fi

	if [ "$hypervisor" == "" ] && [ "$devtree" == "" ] && [ "$kernel_dom0" == "" ] && [ "$kernel_domd" == "" ] && [ "$logo" == "" ] && [ "$xenpolicy" == "" ]; then
		do_mkimg=0
	fi

	if [ $do_oem -eq 1 ] || [ "$omlo" != "" ] || [ "$umlo" != "" ] || [ "$uboot" != "" ] || [ "$hypervisor" != "" ] || [ "$xenpolicy" != "" ] || [ "$devtree" != "" ] || [ "$kernel_dom0" != "" ] || [ "$kernel_domd" != "" ]; then
		do_wrire_op=1
	fi

	if [ "$system" != "" ] || [ "$userdata" != "" ] || [ "$cache" != "" ] || [ "$efs" != "" ] || [ "$rootfs_dom0" != "" ] || [ "$rootfs_domd" != "" ] || [ "$animation" != "" ] || [ "$logo" != "" ]; then

		if ! __dir_exists  ${SCRIPT_AFS_IMAGES_DIR} ; then
			return 1
		fi

		do_wrire_op=1
	fi

	# when device slot is not set we will not write to it
	[ "$set_slot" == "" ] && do_wrire_op=0

	if [ $do_mkimg -eq 0 ] && [ $do_wrire_op -eq 0 ] && [ $eraseuboot -eq 0 ] && [ "$set_slot" == "" ]; then
		echo "---> Warning: nothing to do. Here are 4 operations: 'set device slot', 'eraseuboot', 'mkimg' and 'write to device'"
		echo "---> For 'write to device' operation '[qspi/bp0/emmc/sata]_slot' parametr is mandatory"
		return 1
	fi

	# create cache and efs files if necessary
	[ "$cache" != ""   ] && [ ! -f "$cache" ] && do_create_cache=1
	[ "$efs" != ""   ] && [ ! -f "$efs" ] && do_create_efs=1

	if [ $do_create_cache -eq 1 ] || [ $do_create_efs -eq 1 ] ; then
		if ! __util_is_present ${SCRIPT_MAKE_EXT4FS} ; then
			return 1
		fi
	fi

	if [ $do_create_cache -eq 1 ] || [ $do_create_efs -eq 1 ]; then
		rm -Rf ${SCRIPT_AFS_IMAGES_DIR}/tmpfs
		mkdir -p ${SCRIPT_AFS_IMAGES_DIR}/tmpfs

		if [ $do_create_cache -eq 1 ]; then
			echo "---> File "$(basename $cache)" does not exist. Creating it..."
			${SCRIPT_MAKE_EXT4FS}  -s -l 256M -a cache $cache ${SCRIPT_AFS_IMAGES_DIR}/tmpfs/
		fi

		if [ $do_create_efs -eq 1 ]; then
			echo "---> File "$(basename $efs)" does not exist. Creating it..."
			${SCRIPT_MAKE_EXT4FS} -s -l 16M -a efs $efs ${SCRIPT_AFS_IMAGES_DIR}/tmpfs/
		fi

		rm -Rf ${SCRIPT_AFS_IMAGES_DIR}/tmpfs
	fi

	if [ "$rootfs_dom0" != "" ] || [ "$rootfs_domd" != "" ] ; then

		if ! __script_check_root ; then
			return 1
		fi
	fi

	if [ "$rootfs_dom0" != "" ]; then
		if ! __script_create_rootfs_dom_img "${SCRIPT_ROOTSF_DOM0_DIR}" "0" ; then
			return 1
		fi
	fi

	if [ "$rootfs_domd" != "" ]; then
		if ! __script_create_rootfs_dom_img "${SCRIPT_ROOTSF_DOMD_DIR}" "d" ; then
			return 1
		fi
	fi

	if ! __util_is_present $omlo $umlo $uboot $hypervisor $devtree $kernel_dom0 $kernel_domd $system $userdata $cache $efs $rootfs_dom0 $animation $logo ${xenpolicy} ; then
		return 1
	fi

	if [ $do_mkimg -ne 0 ]; then
		echo "---> making uImages..."
		if [ "$hypervisor" != "" ]; then
			tmp="${hypervisor%.*}-uImage"
			echo "---> making '"$(basename $tmp)"'"
			eval "${SCRIPT_MKIMAGE} ${MKIMGFLAGS_XEN} -d ${hypervisor} ${tmp}"
			hypervisor=$tmp
		fi

		if [ "$devtree" != "" ]; then
			tmp="${devtree%.*}-uImage"
			echo "---> making '"$(basename $tmp)"'"
			eval "${SCRIPT_MKIMAGE} ${MKIMGFLAGS_DT} -d ${devtree} ${tmp}"
			devtree=$tmp
		fi

		if [ "$kernel_dom0" != "" ]; then
			tmp="${kernel_dom0%.*}-uImage"
			echo "---> making '"$(basename $tmp)"'"
			eval "${SCRIPT_MKIMAGE} ${MKIMGFLAGS_ZIMG} -d ${kernel_dom0} ${tmp}"
			kernel_dom0=$tmp
		fi

		if [ "$kernel_domd" != "" ]; then
			tmp="${kernel_domd%.*}-uImage"
			echo "---> making '"$(basename $tmp)"'"
			eval "${SCRIPT_MKIMAGE} ${MKIMGFLAGS_ZIMG} -d ${kernel_domd} ${tmp}"
			kernel_domd=$tmp
		fi

		if [ "$logo" != "" ]; then
			tmp="${logo%.*}-uImage"
			echo "---> making '"$(basename $tmp)"'"
			eval "${SCRIPT_MKIMAGE} ${MKIMGFLAGS_LOGO} -d ${logo} ${tmp}"
			logo=$tmp
		fi

		if [ "$xenpolicy" != "" ]; then
			tmp="${xenpolicy%.*}-uImage"
			echo "---> making '"$(basename $tmp)"'"
			eval "${SCRIPT_MKIMAGE} ${MKIMGFLAGS_XENPOLICY} -d ${xenpolicy} ${tmp}"
			xenpolicy=$tmp
		fi
	fi

	if [ $eraseuboot -ne 0 ]; then
		echo "---> xen fastboot erase partitions BP0, xloader, xloader2..."
		${SCRIPT_FASTBOOT} erase BP0
		${SCRIPT_FASTBOOT} erase xloader
		${SCRIPT_FASTBOOT} erase xloader2
	fi

	if [ "$set_slot" != "" ];then

		if ! __script_fastboot_do_oem_cmd ${set_slot} ; then
			return 1
		fi
	fi

	if [ $do_wrire_op -eq 1 ]; then

		if [ "${script_device_model}" != "PANDA5" ] && [ "${set_slot}" == "spi" ]; then
			# wait while QSPI will be erased
			sleep 3
		fi

		if [ $do_oem -eq 1 ]; then
			if ! __script_fastboot_do_oem_cmd format ; then
				return 1
			fi
		fi

		# On panda boot partition is PB0 on BP0 slot
		if [ $do_set_boot_slot -eq 1 ] && [ "${script_device_model}" == "PANDA5" ]; then
			if ! __script_fastboot_do_flash_cmd BP0 $umlo ; then
				return 1
			fi

			if ! __script_fastboot_do_flash_cmd BP0 $omlo ; then
				return 1
			fi
		else
			if ! __script_fastboot_do_flash_cmd xloader $umlo ; then
				return 1
			fi

			if ! __script_fastboot_do_flash_cmd bootloader $omlo ; then
				return 1
			fi
		fi

		if ! __script_fastboot_do_flash_cmd bootloader $uboot ; then
			return 1
		fi

		if ! __script_fastboot_do_flash_cmd hypervisor $hypervisor ; then
			return 1
		fi

		if ! __script_fastboot_do_flash_cmd xenpolicy $xenpolicy ; then
			return 1
		fi

		if ! __script_fastboot_do_flash_cmd ${devtree_partition_name} $devtree ; then
			return 1
		fi

		if ! __script_fastboot_do_flash_cmd boot $kernel_dom0 ; then
			return 1
		fi

		if ! __script_fastboot_do_flash_cmd domd $kernel_domd ; then
			return 1
		fi

		if ! __script_fastboot_do_flash_cmd system $system ; then
			return 1
		fi

		if ! __script_fastboot_do_flash_cmd animation1 $animation ; then
			return 1
		fi

		if ! __script_fastboot_do_flash_cmd logo $logo ; then
			return 1
		fi

		if ! __script_fastboot_do_flash_cmd ${rootfs_dom0_partition_name} $rootfs_dom0 ; then
			return 1
		fi

		if ! __script_fastboot_do_flash_cmd ${rootfs_domd_partition_name} $rootfs_domd ; then
			return 1
		fi

		if ! __script_fastboot_do_flash_cmd userdata $userdata ; then
			return 1
		fi

		if ! __script_fastboot_do_flash_cmd cache $cache ; then
			return 1
		fi

		if ! __script_fastboot_do_flash_cmd efs $efs ; then
			return 1
		fi

		if [ "${script_device_model}" != "PANDA5" ] && [ "${set_slot}" == "spi" ]; then
			# wait QSPI
			sleep 1
		fi
	fi

	echo "---> Done"
	return 0
}

xen_fastboot() {
	__script_display_info

	export SCRIPT_KERNEL_DOM0_OUT_DIR="${xen_kernel_dom0_out_dir}"
	export SCRIPT_KERNEL_DOMD_OUT_DIR="${xen_kernel_domd_out_dir}"
	export SCRIPT_OMAPBOOT_OUT_DIR="${script_omapboot_out_dir}"
	export SCRIPT_UBOOT_OUT_DIR="${script_uboot_dir}"
	export SCRIPT_HYPERVISOR_OUT_DIR="${script_hypervisor_out_dir}"
	export SCRIPT_HYPERVISOR_POLICY_DIR="${script_hypervisor_policy_dir}"
	export SCRIPT_DEVTREE_OUT_DIR="${scripr_devtree_out_dir}"
	export SCRIPT_FASTBOOT="${script_fastboot}"
	export SCRIPT_MKIMAGE="${script_mkimage}"
	export SCRIPT_MAKE_EXT4FS="${script_make_ext4fs}"
	export SCRIPT_ROOTSF_MAKE_EXT4FS="${script_mkfs_ext4}"
	export SCRIPT_AFS_IMAGES_DIR="${script_afs_images_dir}"
	export SCRIPT_ROOTSF_DOM0_DIR="${script_rootfs_dom0_fs_dir}"
	export SCRIPT_ROOTSF_DOMD_DIR="${script_rootfs_domd_fs_dir}"

	__script_fastboot $@
}

# SCRIPT_KERNEL_DOMU_OUT_DIR must be exported
__xen_copy_domu_image_to_domo_initramfs() {
	echo "---> copying domU "${1}"..."

	if ! __util_is_present  ${SCRIPT_KERNEL_DOMU_OUT_DIR}/${1} ; then
		return 1
	fi

	cp ${SCRIPT_KERNEL_DOMU_OUT_DIR}/${1} ${script_kernel_dom_img_in_rootfs_dom0_dir}/DomULinux.img
	if [ $? -ne 0 ]; then
		echo "---> error: can not copy domU "${1}""
		return 1
	fi

	return 0
}

# SCRIPT_AFS_IMAGES_DIR must be exported
# SCRIPT_KERNEL_AFS_DIR must be exported
# SCRIPT_CUR_WORK_DIR must be exported
# SCRIPT_KERNEL_DOMU_OUT_DIR must be exported
__script_make_afs() {
	local do_clean=0
	local systemimage=""
	local userdataimage=""
	local kernel=""
	local copy_domU=0
	local qt5=""
	local update_api_cmd=""
	local tmp_str=""
	local afs_build_dev="full_jacinto6evm-userdebug"
	local build_name="AFS xen"
	local usage_str="---> Usage: "${FUNCNAME[ 1 ]}" [systemimage] [userdataimage] [qt5] [kernel] [opengl] [fillrate] [update-api] [copy_domU] [max_jobs]"
	local max_jobs=1
	local opengl_cmd=""
	local fillrate_cmd=""

	if ! __check_before_make ; then
		return 1
	fi

	while [ "$1" ]
	do
		case "$1" in
			do_clean) do_clean=1;;
			update-api) update_api_cmd=" && make USE_CCACHE=1 update-api ";;
			systemimage) systemimage="systemimage";;
			userdataimage) userdataimage="userdataimage";;
			kernel) kernel="kernel";;
			copy_domU) copy_domU=1;;
			qt5) qt5="qt5";;
			opengl) opengl_cmd=" && cd ${SCRIPT_CUR_WORK_DIR}/device/generic/goldfish/opengl && BUILD_EMULATOR_OPENGL=true BUILD_EMULATOR_OPENGL_DRIVER=true mm -B && cd ${SCRIPT_CUR_WORK_DIR} ";;
			fillrate) fillrate_cmd=" && cd ${SCRIPT_CUR_WORK_DIR}/frameworks/native/opengl/tests/fillrate && mm -B && cd ${SCRIPT_CUR_WORK_DIR} ";;
			max_jobs) max_jobs=${script_core_cnt};;
			*) echo "$usage_str"; return 1;;
		esac
		shift
	done

	if [ "${script_device_model}" == "PANDA5" ]; then
		afs_build_dev="full_omap5panda-userdebug"
	fi

	if [ ${max_jobs} -ne ${script_core_cnt} ]; then
		echo "---> WARNING!!! 'max_jobs' equals to the default value (${max_jobs})."
		echo "---> 'max_jobs' can be increased using [max_jobs] parameter."
	else
		echo "---> Using 'max_jobs' equals to ${max_jobs}."
	fi

	#if [ $do_clean -ne 0 ];  then
	#	echo "---> clean "$build_name
	#	bash -c 'cd '${SCRIPT_CUR_WORK_DIR}' && . build/envsetup.sh && lunch '${afs_build_dev}' && m -j'${max_jobs}' clean'
	#fi

	#bash -c 'cd '${SCRIPT_CUR_WORK_DIR}' && . build/envsetup.sh && lunch '${afs_build_dev}' && cd '${SCRIPT_KERNEL_AFS_DIR}' && mm kernel -j'${max_jobs}

	# We will remove system.img case if targets 'systemimage' are present in command line
	if [ "${systemimage}" != "" ]; then
		tmp_str=${SCRIPT_AFS_IMAGES_DIR}/obj/PACKAGING/systemimage_intermediates
		if [ -d ${tmp_str} ]; then
			echo "---> removing dir '${tmp_str}'"
			rm -Rf ${tmp_str}
		fi

		for tmp_str in "app" "bin" "etc" "fonts" "framework" "lib" "media" "priv-app" "tts" "usr" "vendor" "xbin" ; do
			tmp_str=${SCRIPT_AFS_IMAGES_DIR}/system/$tmp_str
			if [ -d ${tmp_str} ]; then
				echo "---> removing dir '${tmp_str}'"
				rm -Rf ${tmp_str}
			fi
		done

		for tmp_str in "system.img" "system/build.prop" ; do
			tmp_str=${SCRIPT_AFS_IMAGES_DIR}/$tmp_str
			if [ -f ${tmp_str} ]; then
				echo "---> removing file '${tmp_str}'"
				rm ${tmp_str}
			fi
		done
	fi

	# We will remove userdata.img in case if 'userdataimage' target is present in command line
	if [ "${userdataimage}" != "" ]; then
		tmp_str=${SCRIPT_AFS_IMAGES_DIR}/obj/PACKAGING/userdata_intermediates
		if [ -d ${tmp_str} ]; then
			echo "---> removing dir '${tmp_str}'"
			rm -Rf ${tmp_str}
		fi

		tmp_str=${SCRIPT_AFS_IMAGES_DIR}/userdata.img
		if [ -f ${tmp_str} ]; then
			echo "---> removing file '${tmp_str}'"
			rm ${tmp_str}
		fi
	fi

	echo "---> make "$build_name
	bash -c "cd ${SCRIPT_CUR_WORK_DIR} && unset TOPDIR && unset SHELL && . build/envsetup.sh && lunch ${afs_build_dev} ${update_api_cmd} ${opengl_cmd} ${fillrate_cmd} && m ${userdataimage} ${qt5} ${kernel} ${systemimage} -j${max_jobs}"

	if [ $? -ne 0 ]; then
		echo "---> make "$build_name" error..."
		return 1
	fi

	if [ $copy_domU -eq 1 ]; then
		# hard-coded kernel image name for AFS
		if ! __xen_copy_domu_image_to_domo_initramfs "kernel" ; then
			return 1
		fi
	fi

	echo "---> Done"
	return 0
}

xen_make_afs() {
	__script_display_info

	export SCRIPT_CUR_WORK_DIR="${script_afs_dir}"
	export SCRIPT_KERNEL_AFS_DIR="${script_kernel_afs_dir}"
	export CROSS_COMPILE=${script_afs_toolchain}
	export SCRIPT_KERNEL_DOMU_OUT_DIR="${script_afs_images_dir}"
	export SCRIPT_AFS_IMAGES_DIR="${script_afs_images_dir}"

	__script_make_afs $@
}

# SCRIPT_CUR_WORK_DIR must be exported
# CROSS_COMPILE must be exported
# SCRIPT_ROOTSF_DIR must be exported
__script_make_pvaudio() {
	local build_name="pvaudio xen"
	local usage_str="---> Usage: "${FUNCNAME[ 1 ]}" [do_clean] [copy_executable]"
	local do_clean=0
	local copy_executable=0
	local pvaudio_exec=${SCRIPT_CUR_WORK_DIR}/"sndback"
	local alsasetup_exec=${SCRIPT_CUR_WORK_DIR}/"alsasetup"
	local rootfs_exec_path=${SCRIPT_ROOTSF_DIR}/usr/bin

	if ! __check_before_make ; then
		return 1
	fi

	while [ "$1" ]
	do
		case "$1" in
			do_clean) do_clean=1;;
			copy_executable) copy_executable=1;;
			*) echo "$usage_str"; return 1;;
		esac
		shift
	done

	echo "---> "${build_name}" dir: '"${SCRIPT_CUR_WORK_DIR}"'"

	if [ $do_clean -ne 0 ]; then
		echo "---> clean "$build_name
		make -C ${SCRIPT_CUR_WORK_DIR} clean -j${script_core_cnt} 2>&1

		if [ $? -ne 0 ]; then
			echo "---> make "$build_name" error..."
			return 1
		fi

	fi

	echo "---> make "$build_name
	make -C ${SCRIPT_CUR_WORK_DIR} -j${script_core_cnt} 2>&1

	if [ $? -ne 0 ]; then
		echo "---> make "$build_name" error..."
		return 1
	fi

	#echo "---> Coping files..."
	if [ $copy_executable -ne 0 ]; then
		if ! __script_copy_utils ${rootfs_exec_path} ${pvaudio_exec} ${alsasetup_exec} ; then
			return 1
		fi
	fi

	echo "---> Done "$build_name
	return 0
}

xen_make_pvaudio() {
	__script_display_info

	export SCRIPT_CUR_WORK_DIR="${script_pvaudio_dir}"
	export CROSS_COMPILE="${script_pvaudio_toolchain}"
	export SCRIPT_ROOTSF_DIR="${script_rootfs_domd_fs_dir}"

	__script_make_pvaudio $@
}

# SCRIPT_CUR_WORK_DIR must be exported
# CROSS_COMPILE must be exported
# SCRIPT_ROOTSF_DIR must be exported
__script_make_pvdrm() {
	local build_name="pvdrm xen"
	local usage_str="---> Usage: "${FUNCNAME[ 1 ]}" [do_clean] [copy_executable]"
	local do_clean=0
	local copy_executable=0
	local pvdrm_exec=${SCRIPT_CUR_WORK_DIR}/"drmback"
	local rootfs_exec_path=${SCRIPT_ROOTSF_DIR}/usr/bin

	if ! __check_before_make ; then
		return 1
	fi

	while [ "$1" ]
	do
		case "$1" in
			do_clean) do_clean=1;;
			copy_executable) copy_executable=1;;
			*) echo "$usage_str"; return 1;;
		esac
		shift
	done

	echo "---> "${build_name}" dir: '"${SCRIPT_CUR_WORK_DIR}"'"

	if [ $do_clean -ne 0 ]; then
		echo "---> clean "$build_name
		make -C ${SCRIPT_CUR_WORK_DIR} clean -j${script_core_cnt} 2>&1

		if [ $? -ne 0 ]; then
			echo "---> make "$build_name" error..."
			return 1
		fi

	fi

	echo "---> make "$build_name
	make -C ${SCRIPT_CUR_WORK_DIR} -j${script_core_cnt} 2>&1

	if [ $? -ne 0 ]; then
		echo "---> make "$build_name" error..."
		return 1
	fi

	#echo "---> Coping files..."
	if [ $copy_executable -ne 0 ]; then
		if ! __script_copy_utils ${rootfs_exec_path} ${pvdrm_exec} ; then
			return 1
		fi
	fi

	echo "---> Done "$build_name
	return 0
}

xen_make_pvdrm() {
	__script_display_info

	export SCRIPT_CUR_WORK_DIR="${script_pvdrm_dir}"
	export CROSS_COMPILE="${script_pvdrm_toolchain}"
	export SCRIPT_ROOTSF_DIR="${script_rootfs_domd_fs_dir}"

	__script_make_pvdrm $@
}

# SCRIPT_CPIO_FILE must be exported
# SCRIPT_MODULES_INSTALL_DIR must be exported
__script_unpack_cpio() {
	echo "---> unpacking cpio..."
	echo "---> cpio: ${SCRIPT_CPIO_FILE}"
	echo "---> cpio dir: ${SCRIPT_MODULES_INSTALL_DIR}"

	# delete old dir
	if [ -d ${SCRIPT_MODULES_INSTALL_DIR} ]; then
		rm -Rf ${SCRIPT_MODULES_INSTALL_DIR}
		if [ $? -ne 0 ]; then
			echo "---> error: can not delete old dir '${SCRIPT_MODULES_INSTALL_DIR}'"
			return 1
		fi
	fi

	# create new dir
	if [ ! -d ${SCRIPT_MODULES_INSTALL_DIR} ]; then
		mkdir -p ${SCRIPT_MODULES_INSTALL_DIR}
		if [ $? -ne 0 ]; then
			echo "---> error: can not create dir '${SCRIPT_MODULES_INSTALL_DIR}'"
			return 1
		fi
	fi

	if ! __util_is_present ${SCRIPT_CPIO_FILE} ; then
		return 1
	fi

	sh -c 'cd '${SCRIPT_MODULES_INSTALL_DIR}' && cpio -i --no-absolute-filenames' < ${SCRIPT_CPIO_FILE}
	if [ $? -ne 0 ]; then
		echo "---> error: can not unpack '${SCRIPT_CPIO_FILE}'"
		return 1
	fi

	return 0
}

# SCRIPT_CPIO_FILE must be exported
# SCRIPT_MODULES_INSTALL_DIR must be exported
__script_pack_cpio() {
	echo "---> packing cpio..."
	echo "---> cpio: ${SCRIPT_CPIO_FILE}"
	echo "---> cpio dir: ${SCRIPT_MODULES_INSTALL_DIR}"

	if ! __dir_exists  ${SCRIPT_MODULES_INSTALL_DIR} ; then
		return 1
	fi

	find ${SCRIPT_MODULES_INSTALL_DIR} | cpio -o -H newc  > ${SCRIPT_CPIO_FILE}
	if [ $? -ne 0 ]; then
		echo "---> Error packing cpio"
		return 1
	fi

	return 0
}

# SCRIPT_KERNEL_DOM0_DIR must be exported
# SCRIPT_KERNEL_DOMU_DIR must be exported
# SCRIPT_KERNEL_DOM0_BASE_OUT_DIR must be exported
# SCRIPT_KERNEL_DOMU_BASE_OUT_DIR must be exported
# SCRIPT_KERNEL_DOMU_OUT_DIR must be exported
# SCRIPT_MODULES_INSTALL_DOM0_DIR must be exported (for modules)
# SCRIPT_DOMU_CPIO_FILE must be exported (for modules)
# SCRIPT_MODULES_INSTALL_DOMU_DIR must be exported (for modules)
# SCRIPT_KERNEL_AFS_DIR must be exported
# SCRIPT_AFS_DIR must be exported
# SCRIPT_KERNEL_AFS_OUT_DIR must be exported
__script_make_kernels() {
	local clean=""
	local domU_defconfig=""
	local dom0_defconfig=""
	local domU_devtree=""
	local domu_image=""
	local domu_modules=""
	local dom0_modules=""
	local use_domU_from_AFS=0
	local use_dom0=0
	local skip_domU=0
	local usage_str="---> Usage: "${FUNCNAME[ 1 ]}" [do_clean] [do_defconfig] [use_dom0_modules] [use_dom0] ... "$'\n'
	usage_str=$usage_str"    ... [use_domU_devtree] [use_domU_modules] [use_domU_uncompressed] [use_domU_from_AFS] [skip_domU]"

	while [ "$1" ]
	do
		case "$1" in
			do_clean) clean="do_clean";;
			do_defconfig) domU_defconfig="android_omap_domU_defconfig";dom0_defconfig="omap2plus_dom0_defconfig";;
			use_domU_devtree) domU_devtree="dra7-evm-domU.dtb";;
			use_domU_uncompressed) domu_image="Image";;
			use_dom0_modules) dom0_modules="process_modules"; use_dom0=1;;
			use_domU_modules) domu_modules="process_modules";;
			use_domU_from_AFS) use_domU_from_AFS=1;;
			use_dom0) use_dom0=1;;
			skip_domU) skip_domU=1;;
			*) echo "$usage_str"; return 1;;
		esac
		shift
	done

	if [ $skip_domU -eq 0 ]; then

		if [ ${use_domU_from_AFS} -eq 1 ]; then

			if ! __dir_exists ${SCRIPT_KERNEL_AFS_DIR} ; then
				return 1
			fi

			if [  "$domU_devtree" != "" ] || [  "$domu_image" != "" ] || [  "$domu_modules" != "" ]; then
				echo "---> Error: parameter 'use_domU_from_AFS' can not be used with 'use_domU_devtree' or 'use_domU_modules' or 'use_domU_uncompressed'"
				return 1
			fi

			export SCRIPT_CUR_WORK_DIR="${SCRIPT_AFS_DIR}"
			export CROSS_COMPILE=${script_afs_toolchain}
			export SCRIPT_KERNEL_DOMU_OUT_DIR=${SCRIPT_KERNEL_AFS_OUT_DIR}

			if ! __script_make_afs kernel ; then
				return 1
			fi

		else
			[  "$domu_image" == "" ] && domu_image="zImage"

			export CROSS_COMPILE="${script_kernel_domu_toolchain}"
			export SCRIPT_CUR_WORK_DIR="${SCRIPT_KERNEL_DOMU_DIR}"
			export SCRIPT_KERNEL_BASE_OUT_DIR="${SCRIPT_KERNEL_DOMU_BASE_OUT_DIR}"
			export SCRIPT_BUILD_NAME="kernel domU"

			#remove domU zImage and Image if devtree is used
			if [ "$domU_devtree" != "" ]; then
				# change devtree name according to the board
				if [ "${script_device_model}" == "PANDA5" ]; then
					domU_devtree="omap5-uevm-domU.dtb"
				fi

				for i in "zImage" "Image"; do
					if [ -f ${SCRIPT_KERNEL_DOMU_OUT_DIR}/${i} ]; then
						rm ${SCRIPT_KERNEL_DOMU_OUT_DIR}/${i}

						if [ $? -ne 0 ]; then
							echo "---> can not remove file '"${SCRIPT_KERNEL_DOMU_OUT_DIR}"/"${i}"'"
							return 1
						fi
					fi
				done
			fi

			if [ "$domu_modules" != "" ]; then

				# 1. do clean and/or defconfig (if present) (will not do do_make)
				if [ "$clean" != "" ] ||  [ "$domU_defconfig" != "" ]; then
					if ! __script_make_kernel ${clean} ${domU_defconfig} ; then
						return 1
					fi
				fi

				# 2. do make devtree (if present) (will not do do_make)
				if [ "$domU_devtree" != "" ]; then
					if ! __script_make_kernel ${domU_devtree} ; then
						return 1
					fi
				fi

				export SCRIPT_CPIO_FILE=${SCRIPT_DOMU_CPIO_FILE}
				export SCRIPT_MODULES_INSTALL_DIR=${SCRIPT_MODULES_INSTALL_DOMU_DIR}

				# 3. unpack domUinitramfs.cpio
				if ! __script_unpack_cpio ; then
					return 1
				fi

				# 4. do make and install modules (will not do do_make)
				if ! __script_make_kernel ${domu_modules} ; then
					return 1
				fi

				# 5. do pack domUinitramfs.cpio
				if ! __script_pack_cpio ; then
					return 1
				fi

				# 6. do make kernel
				if ! __script_make_kernel do_make ; then
					return 1
				fi

			else
				# just make kernel domU with parameters
				if ! __script_make_kernel do_make ${clean} ${domU_defconfig} ${domU_devtree} ; then
					return 1
				fi
			fi

			#append devtree if it used
			if [ "$domU_devtree" != "" ]; then
				echo "---> Appending domU devtree '"$domU_devtree"'..."
				cat ${xen_kernel_domu_out_dir}/dts/${domU_devtree} >> ${xen_kernel_domu_out_dir}/${domu_image}
			fi

			# Now __script_make_afs automatically calls this function
			if ! __xen_copy_domu_image_to_domo_initramfs ${domu_image} ; then
				return 1
			fi

		fi
	else
		use_dom0=1
	fi

	if [ $use_dom0 -eq 1 ]; then

		export CROSS_COMPILE="${script_kernel_dom0_toolchain}"
		export SCRIPT_CUR_WORK_DIR="${SCRIPT_KERNEL_DOM0_DIR}"
		export SCRIPT_KERNEL_BASE_OUT_DIR="${SCRIPT_KERNEL_DOM0_BASE_OUT_DIR}"
		export SCRIPT_BUILD_NAME="kernel dom0"
		export SCRIPT_MODULES_INSTALL_DIR=${SCRIPT_MODULES_INSTALL_DOM0_DIR}

		if ! __script_make_kernel ${clean} ${domU_defconfig} ${dom0_modules} do_make; then
			return 1
		fi
	fi

	# just in case
	export SCRIPT_MODULES_INSTALL_DIR=""

	echo "---> Done"
	return 0
}

xen_make_kernels() {
	__script_display_info

	export SCRIPT_KERNEL_DOM0_DIR="${script_kernel_dom0_src_dir}"
	export SCRIPT_KERNEL_DOMU_DIR="${script_kernel_domu_src_dir}"
	export SCRIPT_KERNEL_DOM0_BASE_OUT_DIR="${xen_kernel_dom0_base_out_dir}"
	export SCRIPT_KERNEL_DOMU_BASE_OUT_DIR="${xen_kernel_domu_base_out_dir}"
	export SCRIPT_KERNEL_DOMU_OUT_DIR="${xen_kernel_domu_out_dir}"
	export SCRIPT_MODULES_INSTALL_DOM0_DIR="${script_dom0_modules_install_dir}"
	export SCRIPT_DOMU_CPIO_FILE="${script_domu_cpio_file}"
	export SCRIPT_MODULES_INSTALL_DOMU_DIR="${script_domu_cpio_dir}"
	export SCRIPT_AFS_DIR="${script_afs_dir}"
	export SCRIPT_KERNEL_AFS_DIR="${script_kernel_afs_dir}"
	export SCRIPT_KERNEL_AFS_OUT_DIR="${script_afs_images_dir}"
	export SCRIPT_AFS_IMAGES_DIR="${script_afs_images_dir}"

	__script_make_kernels $@
}

# SCRIPT_CUR_WORK_DIR must be exported
# SCRIPT_ROOTSF_DIR must be exported
__xen_copy_hyp_utils_to_initramfs() {
	local src_dirs=("etc" "usr/local/bin" "usr/local/lib" "usr/local/sbin" "var")
	local hypervisor_install_dir=${SCRIPT_CUR_WORK_DIR}"/dist/install"
	local srcdir=""
	local verbal="-v"
	local usage_str="---> Usage: "${FUNCNAME[ 1 ]}" [silent]"
	local err_code=0
	local use_sudo=0

	echo "---> xen core dir: '"${SCRIPT_CUR_WORK_DIR}"'"

	while [ "$1" ]
	do
		case "$1" in
			silent) verbal="";;
			*) echo "$usage_str"; return 1;;
		esac
		shift
	done


	if ! __dir_exists ${SCRIPT_ROOTSF_DIR} ; then
		return 1
	fi


	for srcdir in ${src_dirs[*]} ; do
		if ! __dir_exists ${hypervisor_install_dir}/${srcdir} ; then
			return 1
		fi
	done

	for srcdir in ${src_dirs[*]} ; do
		if [ ! -d ${SCRIPT_ROOTSF_DIR}/${srcdir} ]; then
			if [ $use_sudo -ne 0 ]; then
				sudo mkdir -p ${SCRIPT_ROOTSF_DIR}/${srcdir}
				err_code=$?
			else
				mkdir -p ${SCRIPT_ROOTSF_DIR}/${srcdir}
				err_code=$?

				if [ $err_code -eq 1 ]; then
					echo "---> warning: permission denied, try to copy with 'sudo'..."
					sudo mkdir -p ${SCRIPT_ROOTSF_DIR}/${srcdir}
					err_code=$?

					use_sudo=1
				fi
			fi

			if [ $err_code -ne 0 ]; then
				echo "---> can not create directory '"${SCRIPT_ROOTSF_DIR}"/"${srcdir}"'"
				return 1
			fi
		fi
	done

	echo "---> copying hyp utils to initramfs..."

	for srcdir in ${src_dirs[*]} ; do
		if [ $use_sudo -ne 0 ]; then
			sudo bash -c "cp -Rfa ${verbal} ${hypervisor_install_dir}/${srcdir}/* ${SCRIPT_ROOTSF_DIR}/${srcdir}"
			err_code=$?
		else
			cp -Rfa ${verbal} ${hypervisor_install_dir}/${srcdir}/* ${SCRIPT_ROOTSF_DIR}/${srcdir}
			err_code=$?

			if [ $err_code -eq 1 ]; then
				echo "---> warning: permission denied, try to copy with 'sudo'..."
				sudo bash -c "cp -Rfa ${verbal} ${hypervisor_install_dir}/${srcdir}/* ${SCRIPT_ROOTSF_DIR}/${srcdir}"
				err_code=$?

				use_sudo=1
			fi
		fi

		if [ $err_code -ne 0 ]; then
			echo "---> can not copy files from '"${hypervisor_install_dir}"/"${srcdir}"' to '"${SCRIPT_ROOTSF_DIR}"/"${srcdir}"'"
			return 1
		fi
	done

	echo "---> Done"
	return 0
}

xen_copy_hyp_utils_to_initramfs() {
	__script_display_info

	export SCRIPT_CUR_WORK_DIR="${script_hypervisor_dir}"
	export SCRIPT_ROOTSF_DIR="${script_rootfs_dom0_fs_dir}"

	__xen_copy_hyp_utils_to_initramfs $@

	export SCRIPT_ROOTSF_DIR="${script_rootfs_domd_fs_dir}"
	__xen_copy_hyp_utils_to_initramfs $@

	if [ "${script_device_model}" == "RENESAS" ]; then
		export SCRIPT_ROOTSF_DIR="${script_rootfs_domc_fs_dir}"
		__xen_copy_hyp_utils_to_initramfs $@

		export SCRIPT_ROOTSF_DIR="${script_rootfs_domt_fs_dir}"
		__xen_copy_hyp_utils_to_initramfs $@
	fi
}

# SCRIPT_CUR_WORK_DIR must be exported
# SCRIPT_KERNEL_DIR must be exported
# SCRIPT_TMP_SYSTEM_INSTALL_DIR must be exported
# SCRIPT_OUT_PREFIX_DIR must be exported
__script_make_sgx() {
	local usage_str="---> Usage: "${FUNCNAME[ 1 ]}" [do_clean] [do_install] [debug]"
	local do_clean=0
	local do_install=0
	local build_name="sgx xen"
	local build_type="release"
	local afs_moules_dir=${SCRIPT_TMP_SYSTEM_INSTALL_DIR}/lib/modules
	local sgx_out_dir
	local files_to_copy
	local count
	local i
	local sgx_build_parameters="TARGET_PRODUCT=omap5sevm TARGET_SGX=544es2"


	# just for GLSDK
	sgx_build_parameters=""

	if ! __check_before_make ; then
		return 1
	fi

	while [ "$1" ]
	do
		case "$1" in
			do_clean) do_clean=1;;
			do_install) do_install=1;;
			debug) build_type="debug";;
			*) echo "$usage_str"; return 1;;
		esac
		shift
	done

	echo "---> "${build_name}" dir: '"${SCRIPT_CUR_WORK_DIR}"'"

	if [ $do_install -ne 0 ];  then
		if [ ! -d ${SCRIPT_TMP_SYSTEM_INSTALL_DIR} ]; then
			mkdir -p ${SCRIPT_TMP_SYSTEM_INSTALL_DIR}

			if [ $? -ne 0 ]; then
				echo "---> Error: can not create '${SCRIPT_TMP_SYSTEM_INSTALL_DIR}' dir"
				return 1
			fi
		fi

		if [ ! -d ${afs_moules_dir} ]; then
			mkdir -p ${afs_moules_dir}
			if [ $? -ne 0 ]; then
				echo "---> Error: can not create '${SCRIPT_TMP_SYSTEM_INSTALL_DIR}' dir"
				return 1
			fi
		fi
	fi

	if [ $do_clean -ne 0 ];  then
		echo "---> clean "$build_name
		make -C ${SCRIPT_CUR_WORK_DIR} ARCH=arm KERNELDIR=${SCRIPT_KERNEL_DIR} ${sgx_build_parameters} BUILD=release 2>&1 clean
		make -C ${SCRIPT_CUR_WORK_DIR} ARCH=arm KERNELDIR=${SCRIPT_KERNEL_DIR} ${sgx_build_parameters} BUILD=debug 2>&1 clean
	fi

	echo "---> make "$build_name
	make -C ${SCRIPT_CUR_WORK_DIR} ARCH=arm KERNELDIR=${SCRIPT_KERNEL_DIR} ${sgx_build_parameters} BUILD=${build_type} -j${script_core_cnt} 2>&1

	if [ $? -ne 0 ]; then
		echo "---> make "$build_name" error..."
		return 1
	fi

	if [ $do_install -ne 0 ];  then
		echo "---> install "$build_name" ..."
		sgx_out_dir=${SCRIPT_OUT_PREFIX_DIR}${build_type}/target

		if ! __dir_exists $sgx_out_dir ; then
			return 1
		fi

		# create files list
		files_to_copy=($(sh -c 'cd '${sgx_out_dir}' && find . -maxdepth 1 -type f -name *.ko | cut -c3-'))

		count=${#files_to_copy[*]}

		if [ $count -eq 0 ]; then
			echo "---> Error: modules are absent in the '${sgx_out_dir}'"
			return 1
		fi

		for i in ${files_to_copy[*]}; do
			cp -fv ${sgx_out_dir}/$i ${afs_moules_dir}
		done

	fi

	echo "---> Done "$build_name
	return 0
}

xen_make_sgx_domU() {
	__script_display_info

	export SCRIPT_CUR_WORK_DIR="${script_sgx_build_dir}"
	export CROSS_COMPILE="${script_sgx_toolchain}"
	export SCRIPT_KERNEL_DIR="${script_kernel_domu_base_out_dir}"
	export SCRIPT_TMP_SYSTEM_INSTALL_DIR="${script_tmp_system_install_dir}"
	export SCRIPT_OUT_PREFIX_DIR="${script_sgx_out_prefix_dir}"

	__script_make_sgx $@
}

xen_make_sgx_dom0() {
	__script_display_info

	export SCRIPT_CUR_WORK_DIR="${script_sgx_build_dir}"
	export CROSS_COMPILE="${script_sgx_toolchain}"
	export SCRIPT_KERNEL_DIR="${script_kernel_dom0_base_out_dir}"
	export SCRIPT_TMP_SYSTEM_INSTALL_DIR="${script_rootfs_dom0_fs_dir}"
	export SCRIPT_OUT_PREFIX_DIR="${script_sgx_out_prefix_dir}"

	__script_make_sgx $@
}

xen_make_sgx_domD() {
	__script_display_info

	export SCRIPT_CUR_WORK_DIR="${script_sgx_build_dir}"
	export CROSS_COMPILE="${script_sgx_toolchain}"
	export SCRIPT_KERNEL_DIR="${script_kernel_domd_base_out_dir}"
	export SCRIPT_TMP_SYSTEM_INSTALL_DIR="${script_rootfs_domd_fs_dir}"
	export SCRIPT_OUT_PREFIX_DIR="${script_sgx_out_prefix_dir}"

	__script_make_sgx $@
}

# SCRIPT_TMP_SYSTEM_INSTALL_DIR must be exported
# SCRIPT_AFS_IMAGES_DIR must be exported
# SCRIPT_SIMG2IMG must be exported
# SCRIPT_MAKE_EXT4FS must be exported
__script_repack_system() {
	local system_img=${SCRIPT_AFS_IMAGES_DIR}/system.img
	local system_img_raw=${SCRIPT_AFS_IMAGES_DIR}/system.img.raw
	local mnt_point=${SCRIPT_AFS_IMAGES_DIR}/mnt_point
	local files_to_copy
	local count
	local last_index
	local cur_owners
	local files_owners
	local i
	local my_user="`id -u -n`"

	if ! __dir_exists ${SCRIPT_TMP_SYSTEM_INSTALL_DIR} ; then
		return 1
	fi


	if ! __util_is_present ${system_img} ${SCRIPT_SIMG2IMG} ${SCRIPT_MAKE_EXT4FS} ; then
		return 1
	fi

	if ! __script_check_root ; then
		return 1
	fi

	echo "---> Mounting original system image..."

	if [ -f ${system_img_raw} ]; then
		rm -f ${system_img_raw}
		if [ $? -ne 0 ]; then
			echo "---> can not delete file ${system_img_raw}..."
			return 1
		fi
	fi

	$SCRIPT_SIMG2IMG ${system_img} ${system_img_raw}

	sudo umount $mnt_point >/dev/null 2>/dev/null
	sudo rm -rf $mnt_point
	mkdir $mnt_point
	sudo mount -t ext4 -o loop ${system_img_raw} $mnt_point


	echo "---> Searching all files to install..."
	files_to_copy=($(sh -c 'cd '${SCRIPT_TMP_SYSTEM_INSTALL_DIR}' && find . -type f | cut -c3-'))

	count=${#files_to_copy[*]}

	if [ $count -eq 0 ]; then
		echo "---> Warning. Install dir is empty. Just create new system image."
	else
		echo "---> Finding out owners..."

		last_index=$(($count-1))
		for i in $(seq 0 $last_index); do
			cur_owners=$(stat -c %u:%g $mnt_point/${files_to_copy[$i]} 2>/dev/null)
			if [ $? -ne 0 ]; then
				cur_owners="0:0"
			fi
			files_owners[$i]=$cur_owners
		done

		echo "---> Copying files..."
		sudo cp -Rf ${SCRIPT_TMP_SYSTEM_INSTALL_DIR}/* $mnt_point

		echo "---> Setting owners..."
		for i in $(seq 0 $last_index); do
			sudo chown ${files_owners[$i]} $mnt_point/${files_to_copy[$i]}
		done
	fi

	echo "---> Making new system image..."
	rm -f ${system_img}
	sudo ${SCRIPT_MAKE_EXT4FS} -s -l 512M -a system ${system_img} $mnt_point
	sudo chown $my_user:$my_user ${system_img}

	echo "---> Removing garbage..."
	sudo umount $mnt_point
	sudo rm -rf $mnt_point
	sudo rm -rf ${system_img_raw}

	echo "---> Done"
	return 0
}

xen_reapck_system() {
	__script_display_info

	export SCRIPT_TMP_SYSTEM_INSTALL_DIR="${script_tmp_system_install_dir}"
	export SCRIPT_AFS_IMAGES_DIR="${script_afs_images_dir}"
	export SCRIPT_SIMG2IMG="${script_simg2img}"
	export SCRIPT_MAKE_EXT4FS="${script_make_ext4fs}"

	__script_repack_system $@
}
